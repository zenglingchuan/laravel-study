<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\UserCard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;

class UserCardController extends Controller
{
    private $card;
    private $request;

    public function __construct(Request $request)
    {
        $this->card = new UserCard();
        $this->request = $request;
    }

    public function mould(): JsonResponse
    {
        $input = $this->request->all();
        $validator = Validator::make($input, [
            'card_id' => 'required',
            'url' => 'required',
        ], [
            'card_id.required' => 'card_id必填',
            'url.required' => 'url必填',
        ]);
        if ($validator->fails()) {
            return $this->error($validator->errors()->first());
        }
        $input['user_id'] = getUserId();
        UserCard::create($input);
        return $this->success();
    }
}
