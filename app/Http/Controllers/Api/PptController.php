<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use PhpOffice\PhpPresentation\PhpPresentation;
use PhpOffice\PhpPresentation\IOFactory;
use PhpOffice\PhpPresentation\Style\Color;
use PhpOffice\PhpPresentation\Style\Alignment;

class PptController extends Controller
{
    public function ppt()
    {
        $objPHPPowerPoint = new PhpPresentation();
        $objPHPPowerPoint->getDocumentProperties()->setCreator('PHPOffice')
            ->setLastModifiedBy('PHPPresentation Team')
            ->setTitle('Sample 02 Title')
            ->setSubject('Sample 02 Subject')
            ->setDescription('Sample 02 Description')
            ->setKeywords('office 2007 openxml libreoffice odt php')
            ->setCategory('Sample Category');

        // 4.删除第一页(多页最好删除)
        $objPHPPowerPoint->removeSlideByIndex(0);
        //根据需求 调整for循环
        for ($i = 1; $i <= 3; $i++) {
            //创建幻灯片并添加到这个演示中
            $slide = $objPHPPowerPoint->createSlide();

            //创建一个形状(图)
            $shape = $slide->createDrawingShape();
            $shape->setName('内容图片name')
                ->setDescription('内容图片 描述')
//                ->setPath(WEB_PATH . '/uploads/img/background.jpg')
                ->setPath(storage_path() . "/ppt/background.jpg")
                ->setResizeProportional(false)
                ->setHeight(720)
                ->setWidth(960);

            //创建一个形状(文本)
            $shape = $slide->createRichTextShape()
                ->setHeight(60)
                ->setWidth(960)
                ->setOffsetX(10)
                ->setOffsetY(50);
            $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $textRun = $shape->createTextRun('以后这个就是标题了');
            $textRun->getFont()->setBold(true)
                ->setSize(20)
                ->setColor(new Color('FFE06B20'));


            // 创建一个形状(文本)
            $shape = $slide->createRichTextShape()
                ->setHeight(60)
                ->setWidth(960)
                ->setOffsetX()
                ->setOffsetY(700);
            $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
            $textRun = $shape->createTextRun('时间:2017年10月19号');
            $textRun->getFont()->setBold(true)
                ->setSize(10)
                ->setColor(new Color('FFE06B20'));
        }

        $oWriterPPTX = IOFactory::createWriter($objPHPPowerPoint, 'PowerPoint2007');
        //路径 /uploads/ppt/  必须存在  echo storage_path();die;
        $url = storage_path() . "\ppt\\" . time() . ".pptx";
        //生成PPT
        $oWriterPPTX->save($url);
        echo $url;die;
        //下载PPT
        $this->download($url);
        //删除PPT
        $this->delDir($url);
        exit;


    }

    private function download($file)
    {
        if (file_exists($file)) {
            header("Content-type:application/octet-stream");
            $filename = basename($file);
            header("Content-Disposition:attachment;filename = " . $filename);
            header("Accept-ranges:bytes");
            header("Accept-length:" . filesize($file));
            readfile($file);
        } else {
            echo "<script>alert('文件不存在')</script>";
        }
    }

    //删除文件
    private function delDir($dir)
    {
        unlink($dir);
        closedir($dir);
    }

}