<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\DieCard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;

class DieCardController extends Controller
{
    private $card;
    private $request;

    public function __construct(Request $request)
    {
        $this->card = new DieCard();
        $this->request = $request;
    }

    public function card(): JsonResponse
    {
        $input = $this->request->all();
        $validator = Validator::make($input, [
            'id' => 'required'
        ], [
            'id.required' => 'id必填',
        ]);
        if ($validator->fails()) {
            return $this->error($validator->errors()->first());
        }
        $data = $this->card->getAllData(['identity_id' => $input['id']], [], ['sort' => 'asc']);
        return $this->success($data);
    }

}
