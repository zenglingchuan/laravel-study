<?php
namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Qiniu\Auth;

class FileController extends Controller
{
    protected $ak = '';
    protected $sk = '';
    public function __construct()
    {
        $this->ak = env("QI_NIU_AK");
        $this->sk = env("QI_NIU_SK");
    }

    /**
     * @param Request $request
     * @return JsonResponse 生成上传Token
     */
    public function upload(Request $request): JsonResponse
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'bucket' => 'required'
        ],[
            'bucket.required' => 'bucket必填',
        ]);
        if ($validator->fails()) {
            return $this->error($validator->errors()->first());
        }
        # 实例化七牛云auth类
        $auth = new Auth($this->ak, $this->sk);
        $up_token['up_token'] = $auth->uploadToken($input['bucket']);
        return $this->success($up_token);
    }

}
