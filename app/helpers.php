<?php
use App\Helper\Http\StatusCode;
# 根据当前header头中token直接获取当前用户id
function getUserId()
{
    $token = $_SERVER['HTTP_TOKEN'] ?? "";
    if ($token == "") {
        #  登录之后的用户, token不可能为空, 做该判断是为了进一步预防前端瞎调api,方便调试
        $json['code'] = StatusCode::ERROR;
        $json['msg'] = 'token不能为空';
        $json['data'] = "";
        echo json_encode($json);
        die;
    }
    $token = base64_decode($token);
    $userId = strrev(substr(strrev($token), 1, strpos(strrev($token), "_") - 3));
    return $userId;
}
/**
 * 分页查找数据
 * @param $data object
 * @param $size int 每页条数
 * @return $data array 数组
 */
function page($data)
{
    // 通过分页器实例方法获取附加的分页信息
    $list['page'] = $data->currentPage();
    $list['size'] = $data->count();
    $list['totalSize'] = $data->total();
    $list['totalPage'] = ceil($data->total() / $data->count());
    $list['list'] = $data->items();
    return $list;
}