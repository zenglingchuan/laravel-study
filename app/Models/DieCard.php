<?php


namespace App\Models;

class DieCard extends BaseModel
{
    protected $fillable = [
        'identity_id',
        'title',
        'thumb',
        'size',
        'sort',
    ];

}
