<?php


namespace App\Models;


class UserIdentity extends BaseModel
{
    protected $fillable = [
        'user_id',
        'identity_id',
        'content'
    ];
}
