<?php


namespace App\Models\Server;


use App\Helper\Token\HttpCurl;

class WeChatLogin
{
    private $appId;
    private $appSecret;
    private $http;

    public function __construct()
    {
        $this->appId = env('APP_ID');
        $this->appSecret = env('APP_SECRET');
        $this->http = new HttpCurl();
    }
    /**
     * 微信登录
     * @code  获取 openid, session_key
     * @param $code
     * @return mixed
     */
    public function login($code)
    {
        $grant_type = "authorization_code";
        $url = "https://api.weixin.qq.com/sns/jscode2session?" . "appid=" . $this->appId . "&secret=" . $this->appSecret . "&js_code=" . $code . "&grant_type=" . $grant_type;
        $data = $this->http->httpCurl($url);
        return $data;
    }

}