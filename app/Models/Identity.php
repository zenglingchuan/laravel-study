<?php


namespace App\Models;

class Identity extends BaseModel
{
    protected $fillable = [
        'name',
        'p_id',
        'form_fields'
    ];

}
