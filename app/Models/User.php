<?php

namespace App\Models;


class User extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nick_name',
        'avatar_url',
        'sex',
        'open_id',
        'session_key',
        'phone'
    ];

}
