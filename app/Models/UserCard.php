<?php


namespace App\Models;


class UserCard extends BaseModel
{
    protected $fillable = [
        'user_id',
        'card_id',
        'url'
    ];
}
