<?php


namespace App\Helper\Token;


use Illuminate\Support\Facades\Redis;

class Token
{
    /**
     * 生成用户的token,保持登陆
     * token目前设定3天有效,超过3天,token失效,3天内有登录,重新计算3天,3天后,自动释放无效token,以防token累积
     * @param  $userid  int      用户id
     * @return $token   string   用户token
     * base64_encode() 编码
     * base64_decode() 解码
     */
    public function createToken($prefix, $id)
    {
        $id_rand = rand(10, 99) . $id . rand(0, 9);
        #生成一个包含用户id的key
        $token = base64_encode(MD5($id . uniqid() . rand(00000000, 99999999)) . '_' . $id_rand);
        $key = $prefix . $id;
        $expire = 3 * 24 * 60 * 60;#生命时间
        Redis::setex($key, $expire, $token);
        return $token;
    }

    /**
     * 用户登陆验证,利用key获取管理员id
     * 1天内有登陆再次更改为1天有效key
     * @param  $id   string  管理员id
     * @return null
     */
    public function checkToken($prefix, $id)
    {
        #根据token令牌规则获取用户id
        $key = $prefix . $id;
        $bool = Redis::get($key);
        return $bool;
    }

    # token校验成功, 顺延3天登录时间
    public function increase($prefix, $id, $token)
    {
        $key = $prefix . $id;
        $expire = 3 * 24 * 60 * 60;
        Redis::setex($key, $expire, $token);
    }

    # 删除token
    public function del($prefix, $id)
    {
        $key = $prefix . $id;
        Redis::del($key);
    }
}
