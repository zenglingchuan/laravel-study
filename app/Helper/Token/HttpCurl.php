<?php


namespace App\Helper\Token;


use App\Helper\Http\StatusCode;

class HttpCurl
{
    /**
     * 公共函数
     * 抓取curl
     * 20210319
     * @param $url string 抓取的地址
     * @param $type string 请求方式
     * @param $header array $header[] 头部
     * @param $params array 数组参数
     */
    public function httpCurl($url, $type = 'GET', $header = [], $params = [], $needHeader = False, $followLocation = True)
    {
        try {
            $header[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36';
            $curl = curl_init();
            # CURLOPT_URL: url地址
            curl_setopt($curl, CURLOPT_URL, $url);
            # CURLOPT_RETURNTRANSFER: 设定是否显示头信息
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, True);
            # 设置CURLOPT_FOLLOWLOCATION为true，则会跟踪爬取重定向页面，否则，不会跟踪重定向页面
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, $followLocation);
            # CURLOPT_SSL_VERIFYPEER: 不检测证书
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, False);
            # CURLOPT_SSL_VERIFYHOST: 不检测证书
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, False);
            # CURLOPT_HEADER: 控制是否返回请求头信息
            curl_setopt($curl, CURLOPT_HEADER, $needHeader);
            # CURLOPT_HTTPHEADER: 设置一个header中传输内容的数组
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            # CURLOPT_ENCODING: 解释gzip内容
            curl_setopt($curl, CURLOPT_ENCODING, 'gzip');
            if ($type != 'GET') {
                # CURLOPT_POST: 启用时会发送一个常规的POST请求，类型为：application/x-www-form-urlencoded，就像表单提交的一样。
                curl_setopt($curl, CURLOPT_POST, True);
                # CURLOPT_POSTFIELDS: 全部数据使用HTTP协议中的"POST"操作来发送。要发送文件，在文件名前面加上@前缀并使用完整路径。这个参数可以通过urlencoded后的字符串类似'para1=val1¶2=val2&...'或使用一个以字段名为键值，字段数据为值的数组。如果value是一个数组，Content-Type头将会被设置成multipart/form-data。

                # $data = '{ "userId": "54601", "groupId": 17, "isDelete": 0, "isLine": 1, "isAlarms": "0", "currPage": 1, "pageSize": 10 }'; 如果数据是以对象的形式传过来的, 则不需要http_build_query()来转化
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
            }
            $data = curl_exec($curl);
            if ($data == False) {
                $back_data['code'] = StatusCode::SERVICE_NETWORK;
                $back_data['msg'] = 'Curl error: ' . curl_error($curl);
            } else {
                $back_data['code'] = StatusCode::SUCCESS;
                $back_data['data'] = $data;
            }
            curl_close($curl);
            return $back_data;
        } catch (\Exception $e) {
            $back_data['code'] = StatusCode::SERVICE_NETWORK;
            $back_data['msg'] = 'curl exception: ' . $e->getMessage();
            return $back_data;
        }
    }
}