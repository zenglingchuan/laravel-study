<?php
/**
 * API状态
 */

namespace App\Helper\Http;


class StatusCode
{
    const SUCCESS = 200; # 成功的code
    const ERROR = 422;   # 业务逻辑错误的code
    const LOGIN_ERROR = 401;   # 登录token失效
    const SERVICE_NETWORK = 500;  # 服务器内部错误
    const NOT_FOUND = 404;
    const VERSION_ERROR = 505; #  后端环境版本问题会返回这个错误码
}
