<?php
/**
 * 用户端路由
 */

//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\IdentityController;
use App\Http\Controllers\Api\DieCardController;
use App\Http\Controllers\Api\UserCardController;
use App\Http\Controllers\Api\PptController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

if(env('APP_ENV') =='local')
{
    $middle = [];
}else{
    $middle = ['api.token'];
}
/**
 * 验证登录
 */
Route::group(['namespace' => 'Api','middleware'=>$middle], function () {
    Route::post('/ppt', [PptController::class, 'ppt']);
    Route::post('/moulds', [UserCardController::class, 'mould']);
    Route::get('/cards', [DieCardController::class, 'card']);
    Route::get('/users', [UserController::class, 'user']);
    Route::delete('/logouts', [UserController::class, 'logout']);
    Route::patch('/phones', [UserController::class, 'phone']);
    Route::match(['get','post','delete'],'/identities', [IdentityController::class, 'identity']);
});

/**
 * 不验证登录
 */
Route::group(['namespace' => 'Api'], function () {
    Route::post('/logins', [UserController::class, 'login']);
});

Route::fallback(function () {
   return 123;
});
